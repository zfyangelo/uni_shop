# uni_shop

#### 介绍
uni-app的练手项目

黑马的教程项目，不过自己做了一些样式的调整和小修改

还有要注意的是，这个项目需要配合黑马自己的sql文件以及phpstudy软件来模拟后端服务器，数据才会显示出来

我做这个项目的时候，还不太会后端的东西，所以如果想传自己的数据就自己改他们的sql文件吧

还有，因为这个项目的sql文件年代久远，一些使用的图片资源的链接已经失效了，不过还是可以凑合着用

如果你觉得不不满意，那你就自己改一下它的sql文件吧

sql文件的仓库我也上传出来，就在README.md的最底部

#### 软件架构

uni_shop
├─ .hbuilderx
│  └─ launch.json
├─ App.vue
├─ components
│  ├─ goods-list
│  │  └─ goods-list.vue  // 商品详情封装组件
│  └─ news-item
│     └─ news.vue  // 资讯封装组件
├─ index.html
├─ main.js
├─ manifest.json
├─ pages
│  ├─ cart
│  │  └─ cart.vue // 购物车组价
│  ├─ contact
│  │  └─ contact.vue // 联系我们组件
│  ├─ goods
│  │  └─ goods.vue // 商城组件
│  ├─ goods_detail
│  │  └─ goods_detail.vue // 商品详情组件
│  ├─ index
│  │  └─ index.vue  // 主页组件
│  ├─ news
│  │  └─ news.vue  // 资讯组件
│  ├─ news-detail
│  │  └─ news-detail.vue // 资讯详情组件
│  ├─ pics
│  │  └─ pics.vue  // 社区图片组件
│  └─ vip
│     └─ vip.vue  // 个人账户组件
├─ pages.json
├─ static // 字体和图标文件
├─ uni.scss
├─ uni_modules
│  ├─ uni-goods-nav
│  ├─ uni-icons
│  └─ uni-scss
├─ unpackage
└─ utils
   └─ api.js // 路由文件



#### 软件的展示

![输入图片说明](https://foruda.gitee.com/images/1674633668930805613/e7ce8578_10697570.png "首页.png")
![输入图片说明](https://foruda.gitee.com/images/1674633703228303744/add2d360_10697570.png "商品详情2.png")
![输入图片说明](https://foruda.gitee.com/images/1674633717631988175/baf914ae_10697570.png "关于我们.png")
![输入图片说明](https://foruda.gitee.com/images/1674633734953872569/24083fe4_10697570.png "uni商店.png")
![输入图片说明](https://foruda.gitee.com/images/1674633750114347106/b456725f_10697570.png "社区图片.png")
![输入图片说明](https://foruda.gitee.com/images/1674633778678097921/399f0d93_10697570.png "资讯列表.png")
![输入图片说明](https://foruda.gitee.com/images/1674633792801263163/bb26bd3a_10697570.png "资讯详情.png")

#### 使用说明

1.  git clone该项目
2.  用hbuilderX打开文件夹
3.  在hbuilderX上运行

#### 参与贡献

sql文件及其node配置仓库：https://gitee.com/zfyangelo/php-serve

仓库中有详细教程怎么使用，这里我就不详细描述如何配置了
